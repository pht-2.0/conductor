FROM  nvidia/cuda:10.2-cudnn7-runtime-ubuntu18.04 as base
WORKDIR /
# Install Python and its tools
RUN apt update && apt install -y --no-install-recommends \
    git \
    build-essential \
    python3-dev \
    python3-pip \
    python3-setuptools
RUN pip3 -q install pip --upgrade
# Install all basic packages
RUN pip3 install \
    # Numpy and Pandas are required a-priori
    numpy pandas \
    # PyTorch with CUDA 10.2 support and Torchvision
    torch torchvision \
    # Upgraded version of Tensorboard with more features
    tensorboardX


FROM base


COPY requirements.txt /opt/requirements.txt
COPY conductor /usr/local/lib/python3.6/dist-packages/conductor
COPY conductor/main.py /opt/main.py
# Set default train directories # TODO check if this gets overwritten
ENV TRAIN_DIR=/home/train TRAIN_DATA_DIR=/home/data
RUN python3 -m pip install -r /opt/requirements.txt
RUN python3 -m pip install -e /usr/local/lib/python3.6/dist-packages/conductor
# TODO provide more sophisticated entrypoint
CMD ["python3", "/opt/main.py"]


