import setuptools

setuptools.setup(
    name="conductor",
    version="0.0.1",
    author="Michael Graf",
    author_email="michael.graf3110@gmail.com",
    description="Python library for Interacting with the PHT",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)