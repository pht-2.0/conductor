import pandas as pd
import numpy as np


def find_variables(data, excluded_categorical, excluded_numeric, min_available=20, display=False):
    """
    Find and output variables to include in the data based on different criteria,
    sort into categorical variables
    :param data: base dataframe
    :type data:
    :param excluded_categorical: categorical varaibles to be excluded
    :type excluded_categorical:
    :param excluded_numeric: numeric variables to be included
    :type excluded_numeric:
    :param min_available: min number of available samples
    :type min_available:
    :return: list of numeric variables, list of categorical variables
    :rtype:
    """
    numerical_columns = []
    categorical_columns = []
    for col in data.columns:
        if data[col].dtype in ["float64", "Int64"]:
            if col not in excluded_numeric:
                n_available = np.sum(data[col].notnull())
                # dont include variables with less than the desired number of entries
                if n_available >= min_available:
                    numerical_columns.append(col)
        elif data[col].dtype in ["object", "string", "category"]:
            if col not in excluded_categorical:
                n_available = np.sum(data[col].notnull())
                # dont include variables with less than the desired number of entries
                if n_available >= min_available:
                    categorical_columns.append(col)
    if display:
        print("Included numerical variables:\n\n", numerical_columns)
        print("\n")
        print("Included categorical variables:\n\n", categorical_columns)
    return numerical_columns, categorical_columns


def encode_categorical_variables(df, cat_cols):
    """
    Takes a dataframe and a list of categorical columns, and creates dummy variables for the categorical
    variables leaving numeric variables
    :param df:
    :type df:
    :param cat_cols:
    :type cat_cols:
    :return:
    :rtype:
    """
    dummy_df = pd.DataFrame()
    for col in cat_cols:
        var_dummy = pd.get_dummies(df[col], prefix=col, dtype=float, dummy_na=True)
        dummy_df = pd.concat([dummy_df, var_dummy], axis=1)
    return dummy_df
