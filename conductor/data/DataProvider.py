from abc import ABC, abstractmethod
from pandas.api.types import is_numeric_dtype
import pandas as pd
from sklearn.preprocessing import StandardScaler
from .DataSource import DataSource, ImageFolderDataSource
import pickle
import json
import codecs
import torchvision
import torch
import os
import inspect
import importlib
from typing import List


class DataProvider(ABC):
    @abstractmethod
    def discover(self, discovery_state: dict = None):
        pass

    @abstractmethod
    def provide(self):
        pass


class TabularDataProvider(DataProvider):
    # TODO needs refactor based on yaml config

    """
    Provider for tabular data based on a data source and an optional discovery config. Performs either data discovery
    or provides training data based on configuration options
    """

    def __init__(self, data_source: DataSource, config: dict = None, config_path: str = None):
        self.data_source = data_source
        data_source.load()
        self.config = config

        if config_path and not config:
            with open(config_path, "r") as f:
                self.config = json.load(f)

        if not config_path:
            self.config_path = "data_config.json"
        else:
            self.config_path = config_path

    def discover(self, display=False, validate=True):
        """
        Perform data discovery for tabular data, updating a scaler object for continuous variables and collecting
        classes and class distributions for categorical variables, verify if all requested variables are present.

        Args:
            validate:
            display: boolean parameter controlling whether to display the discovery results, default = False

        Returns:

        """
        if validate:
            self.validate_data_source()

        cat_var_disc = self.discover_cat_vars()
        self.update_discovery_results(cat_var_disc)
        # TODO find target variable class distribution for classification tasks

        return self.config

    def provide(self):
        self.validate_data_source()
        training_data = self.create_training_data()
        return training_data

    def discover_cat_vars(self, display=False):
        """
        Find the categories and number of samples for each category present in the datasource for the selected
        categorical variables

        Args:
            display: print the results to stdout

        Returns:
            dictionary containing the categories and number of samples for each category

        """
        data: pd.DataFrame = self.data_source.data
        discovery_result = {}
        # Process categorical variables
        for var in self.config["variables"]["categorical"]:
            discovery_result[var] = data[var].value_counts().to_dict()

        # Target variable discovery

        # target = {"name": self.data_config["target"]}
        # # Process target variable get class distribution for classification tasks
        # if is_numeric_dtype(data[target["name"]]):
        #     if is_numeric_dtype(data[target["name"]]):
        #         if len(data[var].unique()) == 2:
        #             target["type"] = "binary"
        #         else:
        #             target["type"] = "regression"
        # else:
        #     if len(data[var].unique()) == 2:
        #         target["type"] = "binary"
        #     else:
        #         target["type"] = "multi-class"
        # if target["type"] in ["binary", "multiclass"]:
        #     class_counts = data[target["name"]].value_counts().to_dict()
        #     target["class_counts"] = class_counts
        #
        # discovery_result["target"] = target

        # Print the results of the data discover process
        if display:
            print(discovery_result)

        return discovery_result

    def discover_target_classes(self):
        # TODO implement this
        pass

    def update_discovery_results(self, cat_var_discovery: dict):
        """
        Update and merge the results of a previous discovery process, the standard scaler and categories and counts
        for the categorical variables

        Args:
            cat_var_discovery: results of performing cat var discovery

        Returns:

        """
        discovery_state = self.config["discovery"]["discovery_state"]
        # Update the total number of samples
        samples = len(self.data_source.data)
        discovery_state["total_samples"] = discovery_state.get("total_samples", 0) + samples

        # Process numeric variables using scikit-learn Standardscaler either update or create a new scaler object
        if discovery_state["scaler"]:
            scaler = self.load_scaler_()
        else:
            scaler = StandardScaler()
        scaler.partial_fit(self.data_source.data[self.config["variables"]["numeric"]])

        # Update categories and category counts
        for cat_var in self.config["variables"]["categorical"]:
            discovery_state["cat_var_discovery"][cat_var] = self._merge_cat_var_results(

                discovery_state["cat_var_discovery"].get(cat_var, None),
                cat_var_discovery.get(cat_var, None)
            )
        # Store cat_var discovery
        self.config["discovery"]["discovery_state"] = discovery_state

        # Store updated scaler
        self.save_scaler_(scaler)

        # Update the config file
        with open(self.config_path, "w") as f:
            json.dump(self.config, f, indent=2)

    @staticmethod
    def _merge_cat_var_results(res_1: dict, res_2: dict):
        """
        Merge two cat var discovery results by creating a union of the found categories and updating the number of
        sample for each existing category
        Args:
            res_1:
            res_2:

        Returns:

        """
        if not res_1:
            return res_2
        elif not res_2:
            return res_1
        else:
            merge_result = {}
            # Find the union of the found categories
            merged_categories = set(res_1.keys()).union(set(res_2.keys()))
            for cat in merged_categories:
                merge_result[cat] = res_1.get(cat, 0) + res_2.get(cat, 0)
        return merge_result

    def fill_missing_values(self, data):
        """
        Fill the missing values based on the results of the data discovery. Fills numerical variables with the calculated
        mean and categorical variables with the most frequent category

        Returns:
            pandas Dataframe with the missing values filled

        """
        discovery_state = self.config["discovery"]["discovery_state"]

        scaler: StandardScaler = self.load_scaler_()
        num_var_means = scaler.mean_
        # fill na with calculated means
        num_fill_values = {num_var: num_var_means[i] for i, num_var in enumerate(self.config["variables"]["numeric"])}
        data = data.fillna(num_fill_values)
        # Fill missing values in categorical columns with the most frequent category
        cat_fill_values = {}
        for cat_var in self.config["variables"]["categorical"]:
            cat_fill_values[cat_var] = max(discovery_state["cat_var_discovery"][cat_var],
                                           key=discovery_state["cat_var_discovery"][cat_var].get)

        data = data.fillna(cat_fill_values)
        return data

    def encode_categorical_variables(self, data):
        # TODO improve this
        cat_df = data[self.config["variables"]["categorical"]].copy()
        dummy_df = pd.DataFrame()
        discovery_state = self.config["discovery"]["discovery_state"]
        # Convert variable to categorical dtype and set the categories based on the discovery state
        for var in self.config["variables"]["categorical"]:
            categories = discovery_state["cat_var_discovery"][var].keys()
            cat_df[var] = cat_df[var].astype("category")
            cat_df[var].cat.set_categories(categories, inplace=True)
            dummies = pd.get_dummies(cat_df[var], prefix=var, dtype=float)
            dummy_df = pd.concat([dummy_df, dummies], axis=1)
        return dummy_df

    def create_training_data(self):

        # Get only the selected variables from the data source
        total_vars = self.config["variables"]["numeric"] + self.config["variables"]["categorical"]
        raw_data = self.data_source.data[total_vars]
        # get dataframe with one-hot encoded categorical variables
        imputed_data = self.fill_missing_values(raw_data)

        dummy_df = self.encode_categorical_variables(imputed_data)

        # Scale numerical variables with the standard scaler
        scaler = self.load_scaler_()
        num_df = imputed_data[self.config["variables"]["numeric"]].copy()
        # TODO impute missing values with the mean of the scaler object
        num_df = scaler.transform(num_df)
        num_df = pd.DataFrame(columns=self.config["variables"]["numeric"], data=num_df)
        X = pd.concat([num_df, dummy_df], axis=1)

        y = self.data_source.data[self.config["target"]]
        # Remove samples that do not containg the target variable
        X = X[y.notnull()]
        y = y[y.notnull()]
        return X, y

    def validate_data_source(self):
        """
        Validate the data source against the data config checking that all variables are present and that they have the
        correct types according to the config

        Returns:

        """

        cols = set(self.data_source.data.columns)
        # validate numeric variables
        for num_var in self.config["variables"]["numeric"]:
            if num_var in cols:
                # Throw error if the column in the data source is not a numeric dtype
                if not is_numeric_dtype(self.data_source.data[num_var]):
                    raise ValueError(f"Variable {num_var} should be a numeric dtype but is "
                                     f"{self.data_source.data[num_var].dtype}")
            else:
                raise ValueError(f"{num_var} not found in the data columns")
        # validate categorical columns
        for cat_var in self.config["variables"]["categorical"]:
            if cat_var in cols:
                if is_numeric_dtype(self.data_source.data[cat_var]):
                    raise ValueError(f"{cat_var} is not a supported categorical dtype")
            else:
                raise ValueError(f"{cat_var} not found in the data columns")

    def load_scaler_(self):
        """
        Load the scaler from a base64 encoded string contained in the data config file
        Returns:
            scikit-learn Standard Scaler
        """
        scaler_str = self.config["discovery"]["discovery_state"]["scaler"]
        scaler = pickle.loads(codecs.decode(scaler_str.encode(), "base64"))
        return scaler

    def save_scaler_(self, scaler):
        """
        Store the given scaler as a base64 encoded string in the config

        Args:
            scaler:

        Returns:

        """
        scaler_str = codecs.encode(pickle.dumps(scaler), "base64").decode()
        self.config["discovery"]["discovery_state"]["scaler"] = scaler_str


class TorchImageDataProvider(DataProvider):
    def __init__(self, mode="folder", data_source: DataSource = None, root: str = None, file: str = None,
                 transform: torchvision.transforms.Compose = None, batch_size: int = None, shuffle: bool = None):
        """
        Dataprovider for pytorch models based on images, provide will return a pytorch Dataloader. Can be based on
        either a folder for classical classification tasks. Or a file specifying targets for each of the listed images

        Args:
            mode: controls what the data provider is based on a target file or directory structure
            root: root path of the directory (only used when the mode is folder)
            file: file specifying targets and paths to image files
            transform: torchvision transforms compose object specifying a sequential list of transformations to be
            to be applied to the image data
            batch_size: batch size of mini-batches to be used for training
            shuffle: boolean parameter specifying whether the images should be shuffled
        """
        self.mode = mode
        self.source = data_source
        self.transform = transform

        self.batch_size = batch_size
        self.shuffle = shuffle
        if mode == "folder" and not root:
            raise ValueError("In folder mode the root directory needs to be set")
        if mode == "file" and not file:
            raise ValueError("In file mode the target file needs to be specified")
        if root:
            self.make_data_source(root, file)

    def discover(self, discovery_state: dict = None):
        """
        Discover the target distribution

        Returns:

        """
        print("Executing discovery")
        # Total number of samples
        n_samples = len([name for name in os.listdir(self.source.load()) if os.path.isfile(name)])
        print(f"Total number of samples: {n_samples}")
        discovery_result = {"n_samples": n_samples}

        # Discover class distribution

        # TODO merge discovery results across stations

        return discovery_result

    def make_data_source(self, root: str, file: str):
        """
        Generate a datasource based if only a root path and not a data source is given to the data provider

        Args:
            root: path to the root directory containing subdirectories named after classes
            file: (not implemented) path to file containing filepaths of images and their respective targets

        Returns:

        """
        if root and not file:
            self.source = ImageFolderDataSource(root=root)
        else:
            # TODO
            raise NotImplementedError("Implement different data sources")

    def validate_data_source(self):
        # TODO
        pass

    def provide(self, batch_size=None, shuffle=True, num_workers=1):
        """
        Creates a pytorch data loader based on the given configuration, including transformations

        Args:
            batch_size: batch size of the created data loader
            shuffle: shuffle the training samples
            num_workers: number of workers to use for loading the data

        Returns:
            Pytorch Dataloader
        """
        if batch_size:
            self.batch_size = batch_size
        if shuffle:
            self.shuffle = shuffle
        if self.mode == "folder":
            ds = torchvision.datasets.ImageFolder(root=self.source.load(), transform=self.transform)
            dl = torch.utils.data.DataLoader(ds, batch_size=self.batch_size, shuffle=self.shuffle,
                                             num_workers=num_workers)
            return dl
        else:
            # TODO
            raise NotImplementedError("Implement this")

    def setup_with_config(self, dp_config: dict):
        # TODO
        self.make_transform(dp_config["transforms"])

    def make_transform(self, transform_config: dict):
        """
        Create a torchvision.transform compose object based on a configuration file and set it as this instances
        transform object

        Args:
            transform_config (dict):

        Returns:

        """
        torch_module = importlib.import_module("torchvision.transforms.transforms")
        mod_dict = torch_module.__dict__
        transforms = []
        for t, args in transform_config.items():
            # If arguments are given pass them to the found transform functions
            if args:
                trans = mod_dict[t](**args)
            else:
                trans = mod_dict[t]()
            transforms.append(trans)
        transform = torchvision.transforms.Compose(transforms)
        self.transform = transform
        return transform

    def serialize_to_config(self):
        """
        Serializes a data provider to a dictionary that can be written to train configuration file

        Returns:
            dict: Dictionary containing the configuration to load a data provider
        """
        dp_config = {"provider": {"type": "TorchImageDataProvider",
                                  "batch_size": self.batch_size,
                                  "shuffle": self.shuffle,
                                  "transforms": self._serialize_transforms()}}
        classes = self._get_classes()
        return dp_config, classes

    def _serialize_transforms(self):
        """
        Serialize the torchvision transform compose object describing the image transformation that the data loader
        applies to the given images

        Returns:
            dict: Dictionary containing the the transformations and their arguments
        """
        transform_dict = {}
        for t in self.transform.transforms:
            members = dict(inspect.getmembers(t))
            t_name = t.__class__.__name__
            args = members["__dict__"]
            transform_dict[t_name] = args
        return transform_dict

    def _get_classes(self):
        """
        Return the classes included in this data provider

        Returns:
            List[str]: list of class names
        """
        if self.mode == "folder":
            classes = [f.name for f in os.scandir(os.path.abspath(self.source.load()))]
        else:
            raise NotImplementedError("Serialization of classes only imported in folder mode")

        return classes
