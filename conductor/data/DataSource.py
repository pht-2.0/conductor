from abc import ABC, abstractmethod
import pandas as pd
import os
from datetime import datetime
from typing import List
from pandas.api.types import is_numeric_dtype, is_datetime64_any_dtype
from sqlalchemy.engine import Connection


class DataSource(ABC):
    """
    Base Class for a data source that can be connected to a DataProvider.
    """

    def __init__(self):
        self.data = None

    @abstractmethod
    def load(self):
        """
        Loads to the data from source into a usable format
        Returns:

        """
        pass


class TabularDataSource(DataSource):
    """
    Data Source for tabular data
    """

    def __init__(self):
        """
        Initialize a tabular data source, with the option of defining a number of variables as categorical or if
        discover is set to True (default) and no list of categorical column names is given discover categorical variables
        based on the types of the columns contained in the pandas Dataframe stored in the the data field of this instance

        Args:
            cat_vars: List of column names containing categorical variable
            discover: boolean parameter controlling whether to find categorical variables present in the data
        """
        self.data: pd.DataFrame = None
        # Load data in to data frame
        self.load()

    def load(self):
        pass


class FileDataSource(TabularDataSource):
    """
    Tabular data source based on a single file
    """

    def __init__(self, path: str):
        """
        Create a File data source based on a given path

        Args:
            path: path to file this data source is based on
        """

        self.type = "file"
        self.path = os.path.abspath(path)
        self.file_type = self.path.split(".")[-1]
        super().__init__()

    def load(self):
        """
        Load the file into a pandas dataframe
        
        Returns:

        """
        self.data = self.load_file_into_df()

    def load_file_into_df(self):
        """
        Checks if the filetype is supported and loads it into a pandas Dataframe

        Returns:
            pandas Dataframe based on the given filepath
        """
        if self.file_type == "csv":
            data = pd.read_csv(self.path)
        elif self.file_type == "tsv":
            data = pd.read_csv(self.path)
        elif self.file_type == "feather":
            data = pd.read_feather(self.path)
        else:
            raise ValueError(f"Filetype .{self.file_type} not supported")
        return data


class SQLDataSource(TabularDataSource):
    """

    """

    def __init__(self, query: str, conn: Connection):
        """
        Data source based on a SQL statement and a sqlalchemy connection, will attempt to load the result of executing
        the query into a pandas dataframe

        Args:
            query: SQL string resulting in a table
            conn: sqlalchemy connection to a database

        """

        self.query = query
        self.conn = conn
        super().__init__()

    def load(self):
        """
        Executes the query string on the instances connection and loads the result into a pandas dataframe

        Returns:

        """
        self.data = pd.read_sql(self.query, self.conn)


class ImageFolderDataSource(DataSource):
    """
    Data Source for classification of images, images belonging to a certain class are stored in subdirectory of the
    root directory with the sub directory name specifying the class label.
    """
    def __init__(self, root):
        self.root = root

    def load(self):
        return self.root
