import os
from datetime import datetime
import json
import matplotlib.pyplot as plt
from typing import List
import pandas as pd


class TrainState:
    def __init__(self, path: str, station_id: str = None, train_name: str = None):
        self.path = path
        self.station_id = station_id
        self.train_name = train_name
        self.finished_discovery = True
        self.discovery_state = None
        self.visited = None
        self.state: dict = None
        self.type = None
        # Setup the state based on the path
        self.parse_train_path()

    def parse_train_path(self):
        if os.path.isdir(self.path):
            # TODO more extensive custom logging based on a directory containing an arbitrary number of files
            self.type = "dir"
            pass
        elif self.path.split(".")[-1].lower() == "json":
            self.type = "json"
            if os.path.isfile(self.path):
                with open(self.path, "r") as f:
                    self.state = json.load(f)
                    self.discovery_state = self.state["discovery_state"]
                    self.finished_discovery = self.discovery_state["finished"]
            else:
                self.state = {}
        else:
            raise ValueError("State path cannot be processed needs to be a directory or a JSON file")

    def save(self):
        if self.type == "json":
            self.to_json()

    def to_json(self):
        with open(self.path, "w") as f:
            json.dump(self.state, f, indent=2)

    def add_losses(self, losses: List[float]):
        """
        Adds the received list of losses to the train state paired with the current station id

        Args:
            losses: list of losses returned by a model

        Returns:

        """
        # TODO improve tensor to float conversion
        loss_list = [{"station": self.station_id,
                      "loss": loss} for loss in losses]
        self.state["performance"]["loss"] = self.state["performance"]["loss"] + loss_list

    def plot_loss(self):
        """
        Plot the running loss split for each station based on the train_state.json file

        Returns:

        """
        loss_df = pd.DataFrame(self.state["performance"]["loss"])
        for station, grp in loss_df.groupby("station"):
            plt.plot(grp["loss"], label=f"Station {station}")
        plt.title(f"Running loss per station")
        plt.legend()
        plt.show()

    def plot_course(self):
        pass
