import json
import os
import datetime
from conductor.core.Train import Train
from conductor.core.TrainState import TrainState
from conductor.data import available_dps, DataProvider, TabularDataProvider, TorchImageDataProvider
from conductor.analysis.models import DistributedModel, TorchModel
import yaml
import sys
import importlib.util
import importlib
import torch
from torchvision.transforms import Compose


class Conductor:

    def __init__(self, train: Train = None, train_dir: str = None, data_dir: str = None, station_id: str = "tue"):
        self.train = train
        self.train_dir = train_dir
        self.data_dir = data_dir
        self.station_id = station_id

    def run_train(self, train_dir=None, data_dir=None):
        """
        Run this instances train object if it is given other wise first create the train based on the given directories
        and then run it

        Args:
            train_dir: directory containing the train configuration file and other required files for the train
            data_dir: directory containing the data to be used when training the model

        Returns:

        """

        # If this instances train is not set attempt to build it based on two given directories
        if not self.train:
            if train_dir and data_dir:
                self.make_train(train_dir, data_dir)
            elif self.train_dir and self.data_dir:
                self.make_train(self.train_dir, self.data_dir)
            else:
                raise ValueError("Neither train nor train directories are set")

        print(f"Station {self.station_id} Executing train")
        self.train.run()
        # Prepare train for distribution
        self.train.distribute(self.train_dir, self.station_id)

    def build_train(self, model: DistributedModel, data_provider: DataProvider):
        """
        Serialize a given train instance to configuration file and other relevant files and send it to the station to
        prepare a submission to the network.

        Returns:

        """
        # TODO
        pass

    def make_train(self, train_dir, data_dir):
        """
        Create a train and associated data provider based on a directory containing a configuration yaml file and a
        directory containing the data to be used by the created data provider.

        Args:
            train_dir: path to the directory containing the train configuration file as well as potentially other train
            related files such as a saved model or train state
            data_dir: directory containing the data to be loaded and processed by the data provider created based on
            the data provider configuration.

        Returns:

        """
        self.train_dir = train_dir
        self.data_dir = data_dir

        config = self.read_train_config()
        # Initialize the model and state based on the configuration file
        model, state = self.process_train_dir(config["train"], config["train"]["state"])
        dp = self.make_provider(config["train"]["data"]["provider"], data_dir)
        self.train = Train(model=model, data_provider=dp, state=state)

    def process_train_dir(self, train_config: dict, state_config: dict):
        """
        Process a train directory containing at the minimum a train configuration yaml and potentially other files
        such as saved model or train state, based on the configuration parsed from the train config file

        Args:
            train_config: dictionary containing the train configuration specified in the configuration yaml file
            state_config: section of the configuration file specifying how and where the train state is kept

        Returns:

        """
        # Sets up the models in the train
        state = self.process_train_state(state_config, train_config["name"])
        model = self.create_train_model_with_config(train_config["model"])

        return model, state

    def create_train_model_with_config(self, model_config: dict):
        """
        Create a train object based on a model configuration given in a train config yaml file

        Args:
            model_config: dictionary containing the configuration options for a distributed model

        Returns:
            DistributedModel: A subclass instance of a distributed model

        """
        model: DistributedModel = None
        if model_config["type"] == "torch":
            # model = self.make_torch_model(model_config)
            model = TorchModel(config=model_config)
            print(model)

            # If a stored model exists load it
            model_path = os.path.join(self.train_dir, model_config["path"])
            if os.path.isfile(model_path):
                model.load(model_path)

        # TODO add more model types
        return model

    def process_train_state(self, train_state_config: dict, train_name: str):
        """
        Create a Trainstate object based on the configuration file and and load existing state based on the
        path given in the configuration file

        Args:
            train_state_config: dictionary specifying the the behaviour of the logging
            train_name: identifier of the train

        Returns:
            TrainState: TrainState object created from the configuration file

        """
        path = os.path.join(self.train_dir, train_state_config["path"])
        self.logging_level = train_state_config["logging_level"]
        ts = TrainState(path=path, train_name=train_name, station_id=self.station_id)
        return ts

    def make_provider(self, provider_config, data_dir):
        """
        Create a data provider with based on the provider configuration in the train config file and a path to a
        directory containing the requested data.

        Args:
            provider_config:
            data_dir:

        Returns:

        """
        provider = provider_config["type"]
        if provider == "custom":
            # TODO
            raise NotImplementedError("Implement this")
        else:
            if provider not in available_dps:
                raise ValueError("Unsupported Dataprovider type")
            else:
                # Create Dataprovider based on the config
                if provider == "TorchImageDataProvider":
                    transform = self.make_transform(provider_config["transforms"])
                    dp = TorchImageDataProvider(root=data_dir, transform=transform,
                                                batch_size=provider_config["batch_size"])
                    return dp

    @staticmethod
    def make_transform(transform_config: dict):
        """
        Create torchvision image transform compose of image transformation based on the selected transforms and their
        arguments in the train configuration

        Args:
            transform_config: dictionary with the name of the transformation as key and the function arguments as values


        Returns:
            torchvision.transforms.Compose to apply the images in the pytorch data loader
        """

        # Import torchvision transforms module
        torch_module = importlib.import_module("torchvision.transforms.transforms")
        mod_dict = torch_module.__dict__
        transforms = []
        for t, args in transform_config.items():
            # If arguments are given pass them to the found transform functions
            if args:
                trans = mod_dict[t](**args)
            else:
                trans = mod_dict[t]()
            transforms.append(trans)
        transform = Compose(transforms)
        return transform

    def read_train_config(self, config_file: str = "train_config.yaml"):
        """
        Reads yaml configuration file containing the train configuration

        Args:
            config_file(str): path to the yaml file

        Returns:
            A dictionary containing the values parsed from the yaml file
        """
        config_path = config_file
        try:
            # Read the top level config file in the given train directory
            config_path = os.path.abspath(os.path.join(self.train_dir, config_file))
            with open(config_path) as f:
                config = yaml.full_load(f)
            return config
        except FileNotFoundError:
            print(f"Train configuration could not be loaded at {os.path.abspath(config_path)}")
