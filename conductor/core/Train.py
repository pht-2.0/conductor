import os
from conductor.analysis.models import DistributedModel
from conductor.data.DataProvider import DataProvider
from .TrainState import TrainState


class Train:
    """
    Base class for representing a pht train
    """

    def __init__(self, model: DistributedModel = None, data_provider: DataProvider = None, state: TrainState = None,
                 config: dict = None, logging_level: str = "batch"):
        self.model: DistributedModel = model
        self.data_provider = data_provider
        self.state = state
        self.logging_level = logging_level
        if config:
            self.init_with_config(config)
        # TODO validate train
        if not state:
            self.state = TrainState("path")

    def run(self):
        """
        Execute a train based on its state, either run the the algorithm if the discovery phase if finished or execute
        the discovery phase

        Returns:

        """
        # If the discovery is finished train the model
        if self.state.finished_discovery:
            print("Training the model")
            self.model.fit(self.data_provider)
        else:
            # Perform discovery and update the discovery state
            discovery_state = self.data_provider.discover(self.state.discovery_state["state"])
            self.state.discovery_state = discovery_state

    def distribute(self, train_dir, station_id):
        """
        Get the results of the train execution ready for distribution on the PHT network. This includes updating and
        saving the model and the train state

        Args:
            train_dir: the directory in which to store the train results
            station_id: id of the station executing the train

        Returns:

        """

        print("Preparing Train for Distribution")
        # If the train was fully executed store state and model
        if self.state.finished_discovery:
            # If logging is enabled store the monitored information in the state
            if self.logging_level:
                self.state.add_losses(self.model.losses)
                self.state.state["execution_state"]["visited"] += station_id
            self.state.save()
            self.model.distribute(train_dir)
        # After performing discovery store only the state
        else:
            self.state.save()

    def make_train_submission(self, api_key=None):
        """
        Serialize the train to prepare it for submission in combination with a station. Generates a directory with the
        files required for running the train

        Returns:

        """
        # TODO automatically get the files the model is defined in and their dependencies and make a zip? file


    def init_with_config(self, config):
        pass

    def build_model_with_config(self, model_config):
        pass

    def validate_train(self):
        # TODO
        pass


class TorchTrain(Train):
    def __init__(self, model: DistributedModel = None, data_provider: DataProvider = None, state: TrainState = None,
                 config: dict = None, logging_level: str = "batch"):
        super().__init__(model, data_provider, state, config, logging_level)

    def run(self):
        super().run()

    def distribute(self, train_dir, station_id):
        super().distribute(train_dir, station_id)

    def make_train_submission(self, api_key=None):
        super().make_train_submission(api_key)

    def serialize_model(self):
        super().serialize_model()

    def init_with_config(self, config):
        super().init_with_config(config)

    def build_model_with_config(self, model_config):
        super().build_model_with_config(model_config)

    def validate_train(self):
        super().validate_train()