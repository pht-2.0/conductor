# from conductor.core import Conductor
# import json
# import yaml
# import os
# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker
# from conductor.data.DataSource import DataSource, FileDataSource
# from conductor.db.database_models import Base
#
#
# class Station:
#     """
#     Main class of the PHT software that keeps state of db, coordinates subclasses for data handling and execution.
#     Class Methods represent the very high level functionality and communication for distributed learning.
#     """
#     # TODO create class variables maybe
#     def __init__(self, config: (dict, str) = "station_config.yml"):
#         """
#         Initialize a station object based on a given configuration
#
#         Args:
#             config:
#         """
#         if type(config) == str:
#             with open(os.path.abspath(config), "r") as config_file:
#                 print("Reading config")
#                 self.config = yaml.full_load(config_file)
#         elif type(config) == dict:
#             self.config = config
#         else:
#             raise ValueError("No valid config given")
#
#         # Set up relevant values either from config or from arguments for this constructor
#
#         self.id = self.config["station"]["id"]
#         self.address = self.config["station"]["address"]
#         self.port = self.config["station"]["port"]
#         self.engine = None
#         self.session = None
#
#         # Configuration for execution, data handling, security etc for station
#         self.station_config = self.config["station"]
#         self._setup_db()
#
#     def _setup_db(self):
#         db_uri = 'postgresql+psycopg2://{}:{}@{}:{}/conductor'.format(
#             self.config["database"]["postgres"]["user"],
#             self.config["database"]["postgres"]["password"],
#             self.config["database"]["postgres"]["address"],
#             self.config["database"]["postgres"]["port"]
#         )
#
#
#         self.engine = create_engine(db_uri)
#         Base.metadata.create_all(self.engine)
#         self.session = sessionmaker(bind=self.engine)
#
#     def add_data_source(self, data_source: dict):
#         db_data_source = FileDataSource(**data_source)
#         db_data_source.serialize()
#
#
#
#     def validate_train(self, train_dir: str = None, train_state = None):
#         """
#         Validate a directory defining a train. Differentiate between new trains and already
#
#         Args:
#             train_dir:
#
#         Returns:
#
#         """
#         pass
#
#
# if __name__ == '__main__':
#     with open("../example_configs/station_config.yml", "r", ) as tc:
#         conf = yaml.load(tc.read())
#     station = Station(conf)
#     data_path = "../analysis/models/sample_data/houseprice_train.csv"
#     # regression_data_source = FileDataSource("regression", data_path, "full house price data set")
#     # station.add_data_source(regression_data_source)
#
#     data_source = FileDataSource("regression", session=station.session)
#     data_source.load()
#
#     # station.assemble_train(train_config=train_conf)
