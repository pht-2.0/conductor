# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy import Column, Integer, String, DateTime
#
# Base = declarative_base()
#
# """
# Define ORM models for the different objects
# """
#
#
# class DataSource(Base):
#     """
#     ORM for data sources
#     """
#     __tablename__ = "data_sources"
#     id = Column(Integer, primary_key=True)
#     name = Column(String, unique=True)
#     description = Column(String, nullable=True)
#     source = Column(String)
#     type = Column(String)
#     created = Column(DateTime)
#
#
# class TrainState(Base):
#     """
#     ORM for the state of a train
#     """
#     __tablename__ = "train_states"
#     id = Column(Integer, primary_key=True)
#     train_id = Column(Integer)
#
#
# class Train(Base):
#     """
#     ORM for a complete train
#     """
#     __tablename__ = "trains"
#     id = Column(Integer, primary_key=True)
#     name = Column(String)
#     train_dir = Column(String)
#     data_source_id = Column(Integer)
