from .DistributedModel import DistributedModel
from conductor.data import DataProvider
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
import joblib


class RandomForest(DistributedModel):
    def __init__(self, data_provider: DataProvider, config: dict = None, **kwargs):
        """
        Random forest model based on the sklearn implementation, creates either a random forest regressor or classifier
        based on the target variable provided by the data_provider. kwargs are passed directly into configuring the
        random forest object created with sklearn
        Classifier params: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier.fit
        Regression params: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html#sklearn.ensemble.RandomForestRegressor

        Args:
            data_provider: data_provider object that will create training data
            config:
            **kwargs: keyword arguments passed directly to the scikit-learn random forest classes
        """
        self.data_provider = data_provider
        super().__init__(config)
        self.predictor = None

    def save(self, path):
        joblib.dump(self.predictor, path)

    def load(self, path):
        joblib.load(path)

    def setup_with_config(self, model_config: dict, data_config: dict):
        pass

    def fit(self, X, y):
        pass

    def predict(self, data):
        pass

    def distribute(self):
        pass

    def set_config(self, config):
        super().set_config(config)

    def update(self, dist_result):
        pass

    def serialize(self):
        super().serialize()