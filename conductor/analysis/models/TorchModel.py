import torch
import torch.nn as nn
from .DistributedModel import DistributedModel
from conductor.data import DataProvider, TorchImageDataProvider
import os
import inspect
import importlib
import sys


class TorchModel(DistributedModel):
    """
    Distributed Model Wrapper for Pytorch networks

    """

    def __init__(self,
                 net: nn.Module = None,
                 criterion: nn.Module = None,
                 optimizer=None,
                 data_provider: TorchImageDataProvider = None,
                 lr=0.001,
                 momentum=0.9,
                 config: dict = None,
                 batch_size=None,
                 num_workers=2,
                 logging_level="batch"
                 ):
        """
        Distributed Model implementing distributed online learning using the PHT.

        Args:
            net: pytorch module defining a network and implementing a forward function
            criterion: loss function torch criterion
            optimizer: torch optimizer callable
            data_provider: Instance of a TorchImageDataProvider
            lr: learning rate
            momentum: momentum
            config: dictionary containing train configuration
            batch_size: the batch size to use for mini batches
            num_workers: number of threads to use for image loading
        """
        super().__init__(config)
        if config:
            self.setup_with_config(config)
        else:
        # TODO handle optional arguments
            self.data_provider = data_provider
            # TODO Check if the network is initialized
            self.net = net
            self.criterion = criterion
            # TODO improve optimizer initialization
            self.optimizer = optimizer(self.net.parameters(), lr=lr, momentum=momentum)
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.losses = []
        self.logging_level = logging_level

    def save(self, path=None):
        """
        Store the model as a pth file at the give path

        Args:
            path: file the model should be saved to

        Returns:

        """
        if path:
            torch.save(self.net.state_dict(), os.path.join(path, "torch_model.pth"))
        else:
            torch.save(self.net.state_dict(), "./torch_model.pth")

    def load(self, path=None):
        """
        Load a pytorch model from a .pth file and initialize this instances network based on the file's state dictionary

        Args:
            path: path to the model file

        Returns:

        """
        if path:
            self.net.load_state_dict(torch.load(path))
        else:
            self.net.load_state_dict(torch.load("./torch_model.pth"))

    def fit(self, data_provider: DataProvider = None, epochs: int = None, device="cpu"):
        """
        Fit the model using the provided data provider instance or based on this instances data provider.

        Args:
            data_provider: data provider instance that generates training data for the network
            epochs: number of epochs
            device: the device to use for training, defaults to cpu

        Returns:

        """

        if data_provider:
            self.data_provider = data_provider
        if not self.data_provider:
            raise ValueError("No DataProvider configured for this model")
        self.net.to(device)
        # for testing the model locally fit the local data over number of epochs
        if epochs:
            print("Fitting the model...")
            for epoch in range(epochs):
                print(f"Epoch: {epoch + 1}")
                self.fit_(device, epoch)
        # Fit based on train config
        else:
            print("Fitting the model for 1 epoch")
            self.fit_(device)

    def fit_(self, device, epoch=None):
        """
        Iterate over this instances data provider and fit the model

        Args:
            device: the device to use for training
            epoch: current epoch

        Returns:

        """
        running_loss = 0.0
        for i, data in enumerate(self.data_provider.provide(
                batch_size=self.batch_size,
                num_workers=self.num_workers), 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs = data[0].to(device)
            labels = data[1].to(device)

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            outputs = self.net(inputs)
            loss = self.criterion(outputs, labels)
            # Store losses for each batch

            loss.backward()
            # print(net.conv1.weight.grad)
            self.optimizer.step()

            # print statistics
            running_loss += loss.item()
            # TODO change this maybe
            if i % 2000 == 1999:  # print every 2000 mini-batches
                if epoch:
                    print('[%d, %5d] loss: %.3f' %
                          (epoch + 1, i + 1, running_loss / 2000))

            if self.logging_level == "batch":
                # TODO check this
                if i >= 1:
                    self.losses.append(running_loss / i)
        if not epoch:
            print(f"Finished training\nLoss {running_loss / i}")

    def predict(self, data):
        pass

    def distribute(self, train_dir):
        self.save(train_dir)

    def update(self, dist_result):
        pass

    def setup_with_config(self, model_config: dict):
        """
        Initialize a Torchmodel based on a model configuration dictionary

        Args:
            model_config:

        Returns:

        """
        self.net = self._create_torch_network_from_config(model_config["net"])
        assert isinstance(self.net, torch.nn.Module)
        self.criterion = self._create_torch_loss_function(model_config["criterion"])
        assert isinstance(self.criterion, torch.nn.Module)
        optim = self._make_torch_optimizer(model_config["optimizer"])
        # TODO add additional optimizer options
        self.optimizer = optim(self.net.parameters(),
                               lr=model_config["optimizer"]["lr"],
                               momentum=model_config["optimizer"]["momentum"])

    def serialize_to_config(self):
        net_file = self._serialize_net()

    def _serialize_net(self):
        # Get the file the network is defined in
        net_file = inspect.getfile(self.net.__class__)
        print(net_file)
        return net_file

    def _serialize_optimizer(self):
        pass

    def _serialize_loss(self):
        pass

    def _create_torch_network_from_config(self, net_config: dict):
        """
        Create a pytorch network based on the given configuration in the train config file

        Args:
            net_config: dictionary containing the torch network configuration

        Returns:

            subclass of torch.nn.module implementing a forward function
        """
        # Import a network from a file defined in the train config
        if net_config["type"] == "custom":
            # Import a torch network defined as a class from a specified python file
            net_file = os.path.abspath(net_config["file"])
            # Import file as module and load the specified class
            file_module = self.import_file_as_module_(net_file)
            # get the net work from the module by the given name
            net = file_module.__dict__.get(net_config["name"])()
            return net
        else:
            # TODO implement transfer learning utilization
            raise NotImplementedError("Implement this")

    def _make_torch_optimizer(self, optim_config: dict):
        """
        Create a pytorch optimizer based on the options given in the train config file

        Args:
            optim_config: dictionary containing the configuration options for the optimizer

        Returns:
            Pytorch optimizer instance to be later initialized with a network
        """
        if optim_config["type"] == "custom":
            # TODO
            raise NotImplementedError("Implement custom loss functions")
        else:
            torch_module = importlib.import_module("torch.optim")
            mod_dict = torch_module.__dict__
            optim = mod_dict[optim_config["type"]]
            return optim

    def _create_torch_loss_function(self, criterion_config: dict):
        """
        Creates the loss fuction for the network based on the arguments given in the train config file

        Args:
            criterion_config:

        Returns:

        """
        if criterion_config["type"] == "custom":
            # TODO
            raise NotImplementedError("Implement custom loss functions")
        else:
            torch_module = importlib.import_module("torch.nn")
            mod_dict = torch_module.__dict__
            loss = mod_dict[criterion_config["type"]]()
            return loss

    @staticmethod
    def import_file_as_module_(path, obj="net"):
        """
        Import a python file as a python module to dynamically interact with user created models etc

        Args:
            path: path to the python file
            obj: name of the object to import from the file

        Returns:
            the python module
        """
        sys.path.append(path)
        file = os.path.abspath(path)
        # Import file as module and load the specified class
        spec = importlib.util.spec_from_file_location(obj, file)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        return mod