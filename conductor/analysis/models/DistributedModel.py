from abc import ABC, abstractmethod
from conductor.data import DataProvider


class DistributedModel(ABC):
    """
    Base class for a distributed model
    """
    def __init__(self, config: dict = None):
        self.config = config

    @abstractmethod
    def save(self, path):
        pass

    @abstractmethod
    def load(self, path):
        pass

    @abstractmethod
    def setup_with_config(self, model_config: dict):
        pass

    @abstractmethod
    def fit(self, data_provider: DataProvider):
        pass

    @abstractmethod
    def predict(self, data):
        pass

    @abstractmethod
    def distribute(self, train_dir):
        pass

    def set_config(self, config):
        self.config = config

    @abstractmethod
    def update(self, dist_result):
        pass

    @abstractmethod
    def serialize_to_config(self):
        pass

