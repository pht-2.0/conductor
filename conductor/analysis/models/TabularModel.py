from .DistributedModel import DistributedModel
from typing import List
import pandas as pd


class TabularModel(DistributedModel):
    """
    Model for working with tabular data
    """

    def __init__(self, config: dict = None, data: pd.DataFrame = None, target: pd.Series = None,
                 prediction_type: str = None, layers: List[int] = None):
        if config:
            super().__init__(config)
        else:
            self.X = data
            self.y = target
            self.prediction_type = prediction_type


    def save(self, path):
        pass

    def load(self, path):
        pass

    def setup_with_config(self, model_config: dict, data_config: dict):
        pass

    def create_training_data(self, path, target):
        pass

    def fit(self, data_provider):
        pass

    def predict(self, data):
        pass

    def distribute(self):
        pass

    def update(self):
        pass
