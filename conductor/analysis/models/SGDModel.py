from sklearn.impute import SimpleImputer

from . import DistributedModel
from conductor.data import load_data, encode_categorical_variables
from sklearn.preprocessing import StandardScaler
import json
import pandas as pd
from sklearn.linear_model import SGDClassifier, SGDRegressor
from sklearn.model_selection import train_test_split
import numpy as np
import pickle
import datetime
from conductor.data import data_util


class SGDDistributedModel(DistributedModel.DistributedModel):
    def __init__(self, config, cat_vars=None, num_vars=None, target=None, data=None):
        """
        Initialize the model object
        :param data: Pandas dataframe containing the data
        :type data:
        :param config: Dict containing the model configuration
        :type config:
        """
        self.data = data
        self.model_config = config
        self.target = target
        self.cat_vars = cat_vars
        self.num_vars = num_vars
        self.predictor = None
        self.x_train = None
        self.y_train = None

    def save(self, filepath=None):
        pass

    def load(self, filepath):
        pass

    def save_predictor(self, filepath=None):
        """
        Store the models predictor as a pickle object
        :param filepath:
        :type filepath:
        :return:
        :rtype:
        """
        # TODO switch to joblib
        if filepath:
            with open(filepath, "wb") as file:
                pickle.dump(self.predictor, file)
        else:
            with open(f"SGD_model_{datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S')}", "wb") as file:
                pickle.dump(self.predictor, file)

    def load_predictor(self, filepath):
        """
        Load a pickled predictor from the specified file path
        :param filepath:
        :type filepath:
        :return:
        :rtype:
        """
        with open(filepath, "rb") as file:
            self.predictor = pickle.load(file)

    def create_predictor(self, config):
        """
        Create an SGD predictor object based on the given config
        :param config:
        :type config:
        :return:
        :rtype:
        """
        # TODO add more model types for multiclass and regression
        if config["prediction_type"] == "binary_classification":
            self.predictor = SGDClassifier(loss=config.get("loss", "hinge"),
                                           penalty=config.get("penalty", "l1"),
                                           max_iter=100000,
                                           verbose=1
                                           )
        if config["prediction_type"] == "regression":
            self.predictor = SGDRegressor(loss=config.get("loss", "squared_loss"),
                                          penalty=config.get("penalty", "l2"),
                                          max_iter=(10 ** 6)/len(self.data) + 20000,
                                          verbose=1,
                                          alpha=7,
                                          eta0=0.01
                                          )
            # Lasso Predictor for comparison
            # self.predictor = LassoCV(alphas=[1, 0.1, 0.001, 0.0005])

            # Standard ridge regression
            # self.predictor = Ridge()

    def create_training_data_(self, one_hot=True, validation_set=False, binary=False):
        """
        Create training data by one hot encoding categorical columns and
        scaling the input to have zero mean and unit variance
        :return:
        :rtype:
        """
        # TODO find a better solution for this
        data = self.data.dropna(how="all", axis=1)
        dummy_df = encode_categorical_variables(data, self.cat_vars)
        cat_df = self.data[self.cat_vars].copy()
        for col in self.cat_vars:
            cat_df[col] = cat_df[col].astype("category").cat.codes
        # Standardize numerical variables
        num_df = self.data[self.num_vars].copy()
        cols = num_df.columns

        # TODO prepare target for the different model types
        target = data[self.target]

        scaler = StandardScaler()

        imp = SimpleImputer(missing_values=np.nan, strategy="median")
        num_df = imp.fit_transform(num_df)
        num_df = scaler.fit_transform(num_df)
        num_df = pd.DataFrame(columns=cols, data=num_df)

        # data = pd.concat([num_df, cat_df], axis=1)
        data = pd.concat([num_df, dummy_df], axis=1)
        # data = data.dropna(how="all")
        # TODO handle still existing nans better than this
        print(data.shape)
        print(len(target))
        data = data[target.notnull()]
        target = target[target.notnull()]
        # target = target.astype(int)
        for col in data.columns:
            data[col] = data[col].astype("float")
        data = data.fillna(method="pad", axis="columns")
        if validation_set is not None:
            x_train, x_val, y_train, y_val = train_test_split(data, target)
            self.x_train = x_train
            self.y_train = y_train
            return x_train, x_val, y_train, y_val
        else:
            self.x_train = data
            self.y_train = target
            return data, target

    def fit(self, validation_set=None, x_train=None, y_train=None):
        if not self.predictor:
            self.create_predictor(self.model_config)

        if x_train is not None and y_train is not None:
            self.predictor.partial_fit(x_train, y_train)

        if validation_set is not None:
            x_train, x_test, y_train, y_test = self.create_training_data_(validation_set=validation_set)
            self.predictor.fit(x_train, y_train)
            print("\n\n Finished Training: \n\n Displaying Results: \n\n")
            print(self.predictor.score(x_test, y_test))
            # TODO calculate validation score and weight of results and store in results
            # TODO handle convergence and early stopping
            print(self.predictor.get_params())
            print(self.predictor.t_)
        else:
            data, target = self.create_training_data_()
            self.predictor.fit(data, target)
            print("\n\n Finished Training: \n\n Displaying Results: \n\n")
            # print(self.predictor.score(x_test, y_test))
            # TODO calculate validation score and weight of results and store in results
            # TODO handle convergence and early stopping
            print(self.predictor.get_params())
            print(self.predictor.t_)
        self.save(filepath="sgd_classifier")

    def partial_fit(self, validation_set=True, x_train=None, y_train=None):
        """
        Perform one iteration of fit either on the models associated data or on given training data
        :param validation_set:
        :type validation_set:
        :param x_train:
        :type x_train:
        :param y_train:
        :type y_train:
        :return:
        :rtype:
        """
        # Train model with given training data
        if x_train and y_train:
            if not self.predictor:
                self.create_predictor(self.model_config)
                self.predictor.partial_fit(x_train, y_train, classes=self.model_config["classes"])
            else:
                self.predictor.partial_fit(x_train, y_train, classes=self.model_config["classes"])

        # Train model using internal training data
        else:
            if not self.predictor:
                self.create_predictor(self.model_config)
            data, target = self.create_training_data_()
            if self.model_config["prediction_type"] != "regression":
                self.predictor.partial_fit(data, target, classes=self.model_config["classes"])
            else:
                self.predictor.partial_fit(data, target)
            if validation_set:
                x_train, x_test, y_train, y_test = self.create_training_data_(validation_set=validation_set)
                if self.model_config["prediction_type"] != "regression":
                    self.predictor.partial_fit(data, target, classes=self.model_config["classes"])
                else:
                    self.predictor.partial_fit(data, target)
                print(self.predictor.score(x_test, y_test))

    def set_data(self, data):
        self.data = data

    def predict(self, data):
        pass

    def score(self):
        # TODO rework this its nonsense
        data, target = self.create_training_data_()
        score = self.predictor.score(data, target)
        print(score)
        return score

    def export_results(self, show=False):
        """
        Create plots displaying model performance as well as feature importances
        :param show:
        :type show:
        :return:
        :rtype:
        """

        coef_df = pd.DataFrame(data=list(self.x_train.columns), columns=["feature"])
        coef_df["coefs"] = self.predictor.coef_
        print(coef_df)

    def distribute(self):
        pass

    def receive(self):
        pass

    def to_json_(self):
        pass


if __name__ == '__main__':
    df = load_data("../user_data/houseprice_station_2.csv")
    test_df = load_data("../user_data/houseprice_test.csv")
    print(df.info())

    # station2_df = pd.read_csv("../core/station_2_df.csv")
    # station_1_test = pd.read_csv("../core/station_1_test.csv")
    # print(station2_df.info())
    num_vars, cat_vars = data_util.find_variables(df, [], [], display=True)

    with open("../../example_configs/house_price_example.json", "r", encoding="utf8") as tc:
        train_conf = json.loads(tc.read())
    print(train_conf)

    sgd_model = SGDDistributedModel(config=train_conf["model"],
                                    cat_vars=train_conf["variables"]["categorical"],
                                    num_vars=train_conf["variables"]["continuous"],
                                    target=train_conf["target"]
                                    )
    sgd_model.set_data(df)
    sgd_model.fit(validation_set=True)
    sgd_model.export_results()
