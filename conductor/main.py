from conductor import Conductor
import os


if __name__ == '__main__':
    cd = Conductor(train_dir=os.getenv("TRAIN_DIR"),
                   data_dir=os.getenv("TRAIN_DATA_DIR"))
    print(os.getenv("TRAIN_DIR"), os.getenv("TRAIN_DATA_DIR"))
    # TODO Improve this
    cd.make_train(cd.train_dir, cd.data_dir)
    cd.run_train()