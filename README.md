# Conductor
Creation, Testing and Execution of PHT trains

## Python Library
Enable creation of trains and testing with local station.

### Installation
To install the python library in the current local interpreter first clone the repository
```
git clone https://gitlab.com/pht-2.0/conductor.git
```
Navigate into the `conductor` directory and run
```
pip install -e .
```
### Examples
Examples for training a pytorch model using this library can be found in the `test` directory. First download the
*[CIFAR10 Data Set](https://www.cs.toronto.edu/~kriz/cifar.html)* from pytorch and the extract the the pickled images 
into image files. (**This will take a a while**)

 ```bash
cd test
python download_sample_data.py
```

This will extract the binary files as well as the images into 5 subdirectories named `batch_{1..5}` inside the `sample_data/cifar10` 
directory. Each directory will contain contains 10 subdirectories named after the available classes.
Then run one of the available test scripts.

## Docker Image
Docker Image that utilizes the python library (and potentially other tools) to execute a train inside a docker container
```
docker build .
docker tag <image_id> pht/conductor:torch
```
### Execute a train using the container
Pass the train directory as well as the directory containing the data to the container as volumes.
By default the conductor looks for train configuration files in `/home/train` and the training data in `/home/data`.
This can be customized by setting the Environment Variables `TRAIN_DIR` and `TRAIN_DATA_DIR` to the paths where the
volumes are mounted.  
The container will attempt to build a train based on the given directory and train the model using the data provided in
the data directory.
```
docker run -v <path_to_train_dir>:/home/train <path_to_data_dir>:/home/data pht/conductor:torch
```