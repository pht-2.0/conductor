from conductor import Conductor
from time import sleep

TRAIN_DIR = "./example_trains/cifar_train"
BASE_DATA_DIR = "../sample_data/cifar10/cifar-10-batches-py/batch_"


def main():
    """
    Run online learning with pytorch cifar simulating stations and their data with batches of cifar images

    Returns:

    """
    n_stations = 5
    for i in range(n_stations):
        cd = Conductor(
            train_dir=TRAIN_DIR,
            data_dir=BASE_DATA_DIR + str(i +1),
            station_id=str(i)
        )
        cd.make_train(cd.train_dir, cd.data_dir)
        cd.run_train()
        cd.train.state.plot_loss()
        sleep(5)


if __name__ == '__main__':
    main()