import pickle
import os
from PIL import Image
import torchvision

if __name__ == '__main__':
    """
    Download the CIFAR10 Dataset and extract the images from the byte encoded numpy arrays into 5 directories named
    batch_{1..5} each containing subdirectories named after the classes in the CIFAR10 Dataset.
    
    """
    # Download the dataset
    trainset = torchvision.datasets.CIFAR10(root="../sample_data/cifar10", train=True, download=True)
    labels = pickle.load(open("../sample_data/cifar10/cifar-10-batches-py/batches.meta", 'rb'), encoding="ASCII")[
        "label_names"]
    for bn in range(5):
        print(f"Extracting batch {bn + 1}...")
        # Load the pickle file for each batch
        with open(f"../sample_data/cifar10/cifar-10-batches-py/data_batch_{bn + 1}", "rb") as f:
            batch = pickle.load(f, encoding="bytes")
        batch_decoded = {}
        for k,v in batch.items():
            batch_decoded[k.decode("utf-8")] = v

        # Extract the filenames and write the corresponding arrays into an image file
        for i, filename in enumerate(batch_decoded["filenames"]):
            folder = labels[batch_decoded["labels"][i]]
            folder = os.path.join(f"../sample_data/cifar10/cifar-10-batches-py/batch_{bn + 1}", folder)
            os.makedirs(folder, exist_ok=True)
            img = batch_decoded["data"][i].reshape((32,32,3), order="F")
            im = Image.fromarray(img)
            im.save(os.path.join(folder, filename.decode("utf-8")))
