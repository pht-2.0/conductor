from conductor import *
import json
from sklearn.ensemble import RandomForestRegressor


ds_1 = FileDataSource("../../analysis/models/sample_data/station_1_houseprice.csv")
ds_2 = FileDataSource("../../analysis/models/sample_data/station_2_houseprice.csv")
ds_3 = FileDataSource("../../analysis/models/sample_data/station_3_houseprice.csv")



test_ds = FileDataSource("../../analysis/models/sample_data/houseprice_test.csv")
test_x, test_y = TabularDataProvider(data_source=test_ds, config_path="data_config_regression.json").provide()


data_provider_1 = TabularDataProvider(data_source=ds_1, config_path="data_config_regression.json")
# data_provider_1.discover()
x_1, y_1 = data_provider_1.provide()

rf_1 = RandomForestRegressor(n_estimators=100, warm_start=True)
rf_1.fit(x_1, y_1)
print(rf_1.score(test_x, test_y))



data_provider_2 = TabularDataProvider(data_source=ds_2, config_path="data_config_regression.json")
# data_provider_2.discover()
x_2, y_2 = data_provider_2.provide()
rf_1.n_estimators = 200
rf_1.fit(x_2, y_2)
print(rf_1.score(test_x, test_y))


data_provider_3 = TabularDataProvider(data_source=ds_3, config_path="data_config_regression.json")
# data_provider_3.discover()
x_3, y_3 = data_provider_3.provide()

rf_1.n_estimators = 300
rf_1.fit(x_3, y_3)
print(rf_1.score(test_x, test_y))

# combined

print(rf_1.score(test_x, test_y))