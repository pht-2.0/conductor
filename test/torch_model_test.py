import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import os
from conductor.analysis.models import TorchModel
from conductor.data.DataProvider import TorchImageDataProvider
import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim

data_set_dir = "C:\\Datasets\\cifar10\\cifar-10-batches-py"

if __name__ == '__main__':
    transform = transforms.Compose([
        transforms.RandomRotation((-90, -90)),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    classes = ('plane', 'car', 'bird', 'cat',
               'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    batch_1_root = os.path.join(data_set_dir, "batch_1")

    dp = TorchImageDataProvider(mode="folder", root=batch_1_root, transform=transform)
    dl = dp.provide(batch_size=4, num_workers=2)

    # functions to show an image
    def imshow(img):
        img = img / 2 + 0.5  # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
        plt.show()


    # get some random training images
    dataiter = iter(dl)
    images, labels = dataiter.next()
    print(images)

    # show images
    imshow(torchvision.utils.make_grid(images))
    # print labels
    print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


    class Net(nn.Module):
        def __init__(self):
            super(Net, self).__init__()
            self.conv1 = nn.Conv2d(3, 32, 5)
            self.pool = nn.MaxPool2d(2, 2)
            self.conv2 = nn.Conv2d(32, 16, 5)
            self.fc1 = nn.Linear(16 * 5 * 5, 120)
            self.fc2 = nn.Linear(120, 84)
            self.fc3 = nn.Linear(84, 10)

        def forward(self, x):
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = x.view(-1, 16 * 5 * 5)
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)

            return x


    tm = TorchModel(Net(), nn.CrossEntropyLoss(), optim.SGD, data_provider=dp)
    tm.fit(epochs=5)
