from conductor import Conductor


def main():
    cd = Conductor(train_dir="./example_trains/cifar_train",
                   data_dir="../sample_data/cifar10/cifar-10-batches-py/batch_1")
    cd.make_train(cd.train_dir, cd.data_dir)
    cd.run_train()
    cd.train.state.plot_loss()


if __name__ == '__main__':
    main()
