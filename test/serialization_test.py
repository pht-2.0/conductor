from conductor import Conductor
from conductor.data.DataProvider import TorchImageDataProvider
from conductor.analysis.models import TorchModel
import os
import torchvision.transforms as transforms
from cifar_net import Net
import torch.nn as nn
import torch.optim as optim

DATA_DIR = "../sample_data/cifar10/cifar-10-batches-py"

if __name__ == '__main__':
    data_set_dir_1 = os.path.join(DATA_DIR, "batch_1")

    transform = transforms.Compose([
        transforms.RandomRotation((-90, -90)),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    dp = TorchImageDataProvider(mode="folder", root=data_set_dir_1, transform=transform, batch_size=4, shuffle=False)
    print(dp.serialize_to_config())
    net = Net()
    tm = TorchModel(net, nn.CrossEntropyLoss(), optim.SGD, data_provider=dp)
    tm.serialize_to_config()










